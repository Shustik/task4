import React, { useState } from "react";
import s from './TextInput.module.css'

export const TextInput = () => {
    const [value, setRadio] = useState('');
    const [checked, setChecked] = useState('');
    const [selected, setSelected] = useState('')
    const [text, setText] = useState('')
    const [showBlock, setShowBlock] = useState(false)

    const changeCheckbox = (e) => {
        setRadio(e.target.value);
    }

    const handleChange = (e) => {
        e.target.checked && setChecked(checked + ', ' + e.target.value)
    }

    const changeSelect = (e) => {
        setSelected(e.target.value)
    }
    const changeText = (e) => {
        setText(e.target.value)
    }
    const deleteText = () => {
        setText('')
    }

    const getInput = () => {
        showBlock ? setShowBlock(false) : setShowBlock(true)
    }
    return (
        <div className={s.wrapper}>
            <div>
                <div className={showBlock ? s.hidden : s.visible && s.textinput__container}>
                    <div className={s.textinput__name}>
                        <div>Калі ласка, увядзіце тэкст</div>
                        <div>
                            <button onClick={deleteText}>Ачысціць</button>
                        </div>
                    </div>
                    <div className={s.textarea}>
                        <textarea placeholder="Увядзіце тэкст" name="text"
                            onChange={changeText} value={text} cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div className={showBlock ? s.hidden : s.visible && s.checkbox__container}  >
                    <div>
                        <div className={s.checkbox__tittle}>Абярыце чэкбоксы:</div>
                        <div className={s.checkbox}>
                            <input type="checkbox" value='чэкбокс #1'
                                onChange={handleChange} />
                            <label htmlFor="scales">чэкбокс #1</label>
                        </div>
                        <div className={s.checkbox}>
                            <input type="checkbox" value='чэкбокс #2'
                                onChange={handleChange} />
                            <label htmlFor="horns">чэкбокс #2</label>
                        </div>
                        <div className={s.checkbox}>
                            <input type="checkbox" value='чэкбокс #3'

                                onChange={handleChange} />
                            <label htmlFor="horns">чэкбокс #3</label>
                        </div>
                    </div>
                    <div className={s.radio}>
                        <div className={s.checkbox__tittle}>Абярыце пераключальнік:</div>
                        <div className={s.radio__item}>
                            <input type="radio" name="radio"
                                onChange={changeCheckbox}
                                checked={value === "пераключальнік #1" ? true : false}
                                value="пераключальнік #1" />
                            <label htmlFor="contactChoice1">пераключальнік #1</label>
                        </div>
                        <div className={s.radio__item}>
                            <input type="radio" name="radio"
                                onChange={changeCheckbox}
                                checked={value === "пераключальнік #2" ? true : false}
                                value="пераключальнік #2" />
                            <label htmlFor="contactChoice2">пераключальнік #2</label>
                        </div>
                        <div className={s.radio__item}>
                            <input type="radio" name="radio"
                                onChange={changeCheckbox}
                                checked={value === "пераключальнік #3" ? true : false}
                                value="пераключальнік #3" />
                            <label htmlFor="contactChoice3">пераключальнік #3</label>
                        </div>
                    </div>
                    <div>
                        <div className={s.checkbox__tittle}>Абярыце опцыю:</div>
                        <select onChange={changeSelect} name="option" className={s.select}>
                            <option value="">Опцыі</option>
                            <option value="Опцыя №1">Опцыя №1</option>
                            <option value="Опцыя №2">Опцыя №2</option>
                            <option value="Опцыя №3">Опцыя №3</option>
                        </select>
                    </div>
                    
                </div>
                <div>
                        <button className={s.button__submit} onClick={getInput}>
                            {showBlock ? 'Схаваць вынік і паказаць поле ўводу і наладкі!' : 'Паказаць уведзены тэкст і абраныя наладкі!'} </button>
                    </div>
                <div className={showBlock ? `${s.visible} ${s.result}`  : s.hidden}>
                    <div className={s.textinput__name}>
                        <div>Вынік</div>
                    </div>
                    <div className={s.textarea}>
                        <div >
                            <p className={s.textarea__tittle}>Карыстальнік увёў наступны тэкст:</p>
                            <p>{text}</p>
                            <p className={s.textarea__tittle}>Карыстальнік абраў наступныя наладкі:</p>
                            <p> <span className={s.textarea__tittle}>Чэкбоксы:</span>  {checked === '' ? 'карыстальнік не абраў чэкбоксы' : checked}</p>
                            <p> <span className={s.textarea__tittle}>Пераключальнікі:</span>  {value === '' ? 'карыстальнік не націснуў на пераключальнік' : value}</p>
                            <p> <span className={s.textarea__tittle}>Опцыі:</span>  {selected === '' ? 'карыстальнік не абраў опцыю' : selected}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}