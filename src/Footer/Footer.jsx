import React from "react";
import s from './Footer.module.css'

export const Footer = () => {
    return (
        <div className={s.footer__container}>
            <div className={s.footer__text}>
                <p>Мы будзем рады атрымаць зваротную сувязь ад Вас на e-mail <a className={s.link} href="corpus.by@gmail.com">corpus.by@gmail.com</a> .</p>
                <p>Нашы іншыя прататыпы: <a className={s.link} href="corpus.by">corpus.by</a>, <a className={s.link} href="ssrlab.by">ssrlab.by</a> .</p>
                <p>Лабараторыя распазнавання і сінтэзу маўлення, АІПІ НАН Беларусі, 2016—2022</p>
            </div>       
        </div>
    )
}