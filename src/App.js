import './App.css';
import { Footer } from './Footer/Footer';
import { Header } from './header/Header';
import { TextInput } from './TextInput/TextInput';

export const App = () => {
  return (
    <div className="app">
      <div className='app__container'>
        <Header />
        <TextInput />
        <Footer />
      </div>

    </div>
  );
}


