import React from "react";
import logo from '../img/main-logo.png'
import s from './Header.module.css'

export const Header = () => {
    return (
        <header className={s.wrapper}>
            <div className={s.container}>
                <div className={s.container__left}>
                    <div className={s.container__logo}>
                        <img src={logo} alt='#' />
                    </div>
                    <div className={s.container__name}>
                        Дэманстрацыйны сэрвіс
                    </div>
                </div>
                <div className={s.container__right}>
                    <div ><a className={s.question} href="https://ssrlab.by/5208">
                    ?</a></div>
                    <div className={s.button__language}>
                        <div className={s.language__menu}>
                            <select name="language"
                                className={s.select}>
                                <option value="Беларуская">Беларуская</option>
                                <option value="English">English</option>
                                <option value="Русский">Русский</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </header>

    );
}